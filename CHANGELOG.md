# Changelog
All notable changes to are documented here.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.0] - 2019-08-06

This is a "release early, release often" release.  The software is still in alpha state.  User api and user interface are not stable.
